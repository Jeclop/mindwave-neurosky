﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;
using System.IO.Ports;
using NeuroSky.ThinkGear;
using NeuroSky.ThinkGear.Algorithms;
using System.Windows.Forms;

namespace MindWave_INNOVA
{
    class Program
    {
        static Connector connector;
        //static byte poorSig;

        static int minutos;
        static short s_pura;
        static uint delta,theta,alpha1,alpha2,beta1,beta2,gamma1,gamma2;
        static double atencion,meditacion,señal,con;
        static string carpeta;
        static bool ok=false;
        static StringBuilder cvscon = new StringBuilder();
    



         [STAThread]

        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenido a MindWave de Innova!");
            Console.WriteLine("Por favor indique el nombre y ubicacion del archivo que será guardado");
            Thread.Sleep(2000);

           
            cvscon.AppendLine("Raw,Atencion,Meditacion,Delta,Teta,Low Alfa,High Alfa,Low Beta,High Beta,Low Gamma,High Gamma,Señal,Tiempo");

            guardar();
            Console.WriteLine("Cuando ya no requiera tomar mas datos por favor levante el brazo de la diadema");
             Console.WriteLine("o retirela completamente");
            Thread.Sleep(2000);
             connector = new Connector();
            connector.DeviceConnected += new EventHandler(OnDeviceConnected);
            connector.DeviceConnectFail += new EventHandler(OnDeviceFail);
            connector.DeviceValidating += new EventHandler(OnDeviceValidating);

          // Console.WriteLine("Por favor ingrese el tiempo en minutos de recopilacion de datos");
            //Console.Write("Minutos: ");
          //  minutos = Convert.ToInt16(Console.ReadLine());

            connector.ConnectScan("COM3");

            //Para la deteccion de pestañeo se necesita activar la funcion de forma manual


            

            connector.setBlinkDetectionEnabled(true);


           
            Thread.Sleep(300 * 60000);

         /*   SaveFileDialog salvar = new SaveFileDialog();
            salvar.Filter = "Archivos csv(*.csv)|*.csv";
            salvar.Title = "Guardar";
            if (salvar.ShowDialog() == DialogResult.OK)
            {
                carpeta = salvar.FileName;

            }

            salvar.Dispose();

            carpeta = carpeta.Replace("\\", "\\\\");
            File.AppendAllText(carpeta, cvscon.ToString());
             */
            //Llama la funcion que guarda los datos en una hoja de calculo excel
         //   Hexcel();

           
            //Termina la conexion de la diadema con el computador 
            connector.Close();

            //Cierra la ventana de consola
            Environment.Exit(0);

        }
        static void OnDeviceConnected(object sender, EventArgs e)
        {

            Connector.DeviceEventArgs de = (Connector.DeviceEventArgs)e;

            Console.WriteLine("Dispositivo conenctado en el puerto " + de.Device.PortName);

            de.Device.DataReceived += new EventHandler(OnDataReceived);

        }

        static void OnDeviceFail(object sender, EventArgs e)
        {

            Console.WriteLine("Dispositivo no encontrado! :(");

        }

        // Funcion que se activa cuando se esta validando los puertos

        static void OnDeviceValidating(object sender, EventArgs e)
        {

            Console.WriteLine("Validating: ");

        }

      

        static void OnDataReceived(object sender, EventArgs e)
        {

            

            Device.DataEventArgs de = (Device.DataEventArgs)e;
            DataRow[] tempDataRowArray = de.DataRowArray;

            TGParser tgParser = new TGParser();
            tgParser.Read(de.DataRowArray);

           
            for (int i = 0; i < tgParser.ParsedData.Length; i++)
            {


                if (tgParser.ParsedData[i].ContainsKey("Raw"))
                {
                  s_pura=(System.Convert.ToInt16(tgParser.ParsedData[i]["Raw"]));
                }

                if (tgParser.ParsedData[i].ContainsKey("PoorSignal"))
                {
                  Console.WriteLine("Señal:" + tgParser.ParsedData[i]["PoorSignal"]);
                  señal=(System.Convert.ToDouble(tgParser.ParsedData[i]["PoorSignal"]));

                  if (señal >= 180)
                  {
                      con = con + 1;

                      if (con == 5)
                      {
                          
                          File.AppendAllText(carpeta, cvscon.ToString());
                          connector.Close();
                          Environment.Exit(0);
                          // guardar();

                      }


                  }
                  else { con = 0; }
                }


                if (tgParser.ParsedData[i].ContainsKey("Attention"))
                {
                  atencion = (tgParser.ParsedData[i]["Attention"]);
                }


                if (tgParser.ParsedData[i].ContainsKey("Meditation"))
                {                   
                  meditacion=(tgParser.ParsedData[i]["Meditation"]);
                }


                if (tgParser.ParsedData[i].ContainsKey("EegPowerDelta"))
                {                                   
                  delta=(System.Convert.ToUInt32(tgParser.ParsedData[i]["EegPowerDelta"]));
                }

                if (tgParser.ParsedData[i].ContainsKey("EegPowerTheta"))
                {              
                  theta=(System.Convert.ToUInt32(tgParser.ParsedData[i]["EegPowerTheta"]));
                }

                if (tgParser.ParsedData[i].ContainsKey("EegPowerAlpha1"))
                {
                  alpha1=(System.Convert.ToUInt32(tgParser.ParsedData[i]["EegPowerAlpha1"]));
                }

                if (tgParser.ParsedData[i].ContainsKey("EegPowerAlpha2"))
                {                    
                  alpha2=(System.Convert.ToUInt32(tgParser.ParsedData[i]["EegPowerAlpha2"]));
                }

                if (tgParser.ParsedData[i].ContainsKey("EegPowerBeta1"))
                {
                  beta1=(System.Convert.ToUInt32(tgParser.ParsedData[i]["EegPowerBeta1"]));
                }

                if (tgParser.ParsedData[i].ContainsKey("EegPowerBeta2"))
                {
                  beta2=(System.Convert.ToUInt32(tgParser.ParsedData[i]["EegPowerBeta2"]));
                }

                if (tgParser.ParsedData[i].ContainsKey("EegPowerGamma1"))
                {
                  gamma1=(System.Convert.ToUInt32(tgParser.ParsedData[i]["EegPowerGamma1"]));
                }

                if (tgParser.ParsedData[i].ContainsKey("EegPowerGamma2"))
                {
                  gamma2=(System.Convert.ToUInt32(tgParser.ParsedData[i]["EegPowerGamma2"]));

                  cvscon.AppendLine(Convert.ToString(s_pura) + "," + Convert.ToString(atencion) + "," + Convert.ToString(meditacion) + "," +
                                    Convert.ToString(delta) + "," + Convert.ToString(theta) + "," + Convert.ToString(alpha1) + "," + Convert.ToString(alpha2) + "," +
                                    Convert.ToString(beta1) + "," + Convert.ToString(beta2) + "," + Convert.ToString(gamma1) + "," + Convert.ToString(gamma2) + "," +
                                    Convert.ToString(señal)+"," + DateTime.Now.ToLongTimeString());
                 }

                                        

            }
           

        }

        static void guardar() {
            Thread.Sleep(60);
            SaveFileDialog salvar = new SaveFileDialog();
            salvar.Filter = "Archivos csv(*.csv)|*.csv";
            salvar.Title = "Guardar";
            if (salvar.ShowDialog() == DialogResult.OK)
            {
                carpeta = salvar.FileName;

            }

            salvar.Dispose();

            carpeta = carpeta.Replace("\\", "\\\\");
            //File.AppendAllText(carpeta, cvscon.ToString());

           // connector.Close();
          //  Environment.Exit(0);
        
        
        }
    }
}
